const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TSConfigPathsWebpackPlugin = require('tsconfig-paths-webpack-plugin');

const CURRENT_DIRECTORY = path.resolve();

module.exports = {
  target: 'web',
  entry: [
    path.join(CURRENT_DIRECTORY, 'src/index.tsx'),
    path.join(CURRENT_DIRECTORY, 'src/customizations/entrypoints.scss'),
  ],
  output: {
    path: path.resolve('dist'),
    filename: '[name].js',
    chunkFilename: '[name].bundle.js',
    clean: true,
  },
  resolve: {
    modules: [path.resolve('src'), 'node_modules'],
    extensions: ['*', '.js', '.jsx', '.ts', '.tsx', '.scss'],
    symlinks: false,
    plugins: [
      new TSConfigPathsWebpackPlugin(),
    ],
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: '/node_modules/',
    },
    {
      test: /\.s?css$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader',
        'postcss-loader',
        {
          loader: 'sass-resources-loader',
          options: {
            resources: path.join(CURRENT_DIRECTORY, 'src/customizations/resources.scss'),
          },
        },
      ],
    },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(CURRENT_DIRECTORY, 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'styles/[name].[contenthash].css',
      chunkFilename: 'styles/[name].[contenthash].css',
    }),
  ],
};
