const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const common = require('./webpack.common');

const CURRENT_DIRECTORY = path.resolve();

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new CopyWebpackPlugin(
      {
        patterns: [
          {
            from: path.join(CURRENT_DIRECTORY, 'resources/server'),
            to: path.join(CURRENT_DIRECTORY, 'dist/server'),
          },
        ],
      },
    ),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  ],
});
