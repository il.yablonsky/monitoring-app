const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');

const common = require('./webpack.common');

const CURRENT_DIRECTORY = path.resolve();

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  output: {
    publicPath: '/assets/',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
  ],
  devServer: {
    contentBase: path.join(CURRENT_DIRECTORY, 'dist'),
    port: 3000,
    watchContentBase: true,
    publicPath: '/assets/',
    historyApiFallback: true,
  },
});
