const path = require('path');
const express = require('express');

const app = express();
const port = process.env.PORT || 3003;
const ROOT_DIRECTORY = path.resolve();

app.use('/assets', express.static(`${ROOT_DIRECTORY}/dist`));

app.get('*', (req, res) => {
  res.sendFile(path.join(`${ROOT_DIRECTORY}/dist/index.html`), (err) => {
    if (err) {
      res.status(500).send(err);
    }
  });
});

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Server is listening on ${port} port`));
