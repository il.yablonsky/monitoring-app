import { createReducer } from 'deox';

import {
  setAvlUnits,
  setUnits,
} from '@/actions/units';
import { IUnits } from '@/interfaces/state/units';
import { REDUCER_TYPES } from '@/constants';
import { getCommonReducers, getInitialState } from './common';

const initialState = getInitialState({
  avlUnits: [],
  units: {},
}) as IUnits;

export const units = createReducer(
  initialState,
  handleAction => ([
    handleAction(setAvlUnits, (state, { payload }) => ({
      ...state,
      avlUnits: payload,
    })),
    handleAction(setUnits, (state, { payload }) => ({
      ...state,
      units: payload,
    })),
    ...getCommonReducers(REDUCER_TYPES.UNITS, initialState, handleAction),
  ]),
);
