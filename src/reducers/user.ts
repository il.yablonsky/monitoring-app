import { createReducer } from 'deox';

import { setUser } from '@/actions/user';
import { REDUCER_TYPES } from '@/constants';
import { getCommonReducers, getInitialState } from './common';

const initialState = getInitialState({
  userName: '',
});

export const user = createReducer(
  initialState,
  handleAction => ([
    handleAction(setUser, (state, { payload }) => ({
      ...state,
      ...payload,
    })),
    ...getCommonReducers(REDUCER_TYPES.USER, initialState, handleAction),
  ]),
);
