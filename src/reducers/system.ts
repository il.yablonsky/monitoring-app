import { createReducer } from 'deox';

import {
  globalError,
  redirectTo,
  setAccessToken,
  setGlobalInProgressStatus,
  clearErrors,
  clearSystem,
  clearRedirectTo,
} from '@/actions/system';
import { ERRORS } from '@/constants';

const initialState = {
  globalError: {},
  globalInProgressStatus: false,
  is404: false,
  redirectTo: '',
  accessToken: '',
};

export const system = createReducer(initialState, handleAction => ([
  handleAction(globalError, (state, { payload: { error = ERRORS.UNDEFINED, status } }) => ({
    ...state,
    globalError: {
      ...state.globalError,
      errorId: error,
      status,
    },
  })),
  handleAction(redirectTo, (state, { payload }) => ({
    ...state,
    redirectTo: payload,
  })),
  handleAction(setAccessToken, (state, { payload }) => ({
    ...state,
    accessToken: payload,
  })),
  handleAction(setGlobalInProgressStatus, (state, { payload }) => ({
    ...state,
    globalInProgressStatus: payload,
  })),
  handleAction(clearRedirectTo, state => ({
    ...state,
    redirectTo: '',
  })),
  handleAction(clearErrors, state => ({
    ...state,
    globalError: {},
  })),
  handleAction(clearSystem, () => ({
    ...initialState,
  })),
]));
