import { createReducer } from 'deox';

import { setAuthStatus } from '@/actions/auth';
import { REDUCER_TYPES } from '@/constants';
import { getCommonReducers, getInitialState } from './common';

const initialState = getInitialState({
  isAuthorized: false,
});

export const auth = createReducer(
  initialState,
  handleAction => ([
    handleAction(setAuthStatus, (state, { payload }) => ({
      ...state,
      isAuthorized: payload,
    })),
    ...getCommonReducers(REDUCER_TYPES.AUTH, initialState, handleAction),
  ]),
);
