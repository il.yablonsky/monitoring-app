import { combineReducers } from 'redux';

import { system } from './system';
import { user } from './user';
import { auth } from './auth';
import { units } from './units';

export const rootReducer = combineReducers({
  system,
  user,
  auth,
  units,
});
