import { createStore, applyMiddleware, Action } from 'redux';
import { createEpicMiddleware, Epic } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ajax } from 'rxjs/ajax';

import { rootReducer } from '@/reducers';
import { rootEpic } from '@/epics';
import { IS_MODE_DEV } from '@/constants';

export const configureStore = () => {
  const epicMiddleware = createEpicMiddleware({
    dependencies: { ajax },
  });

  const store = createStore(
    rootReducer,
    IS_MODE_DEV
      ? composeWithDevTools(applyMiddleware(epicMiddleware))
      : applyMiddleware(epicMiddleware),
  );

  epicMiddleware.run(rootEpic as Epic<Action<any>>);

  return store;
};

export const store = configureStore();
