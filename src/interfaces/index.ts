export type Nulish<T> = T | null;
export type Entity<T> = { id: string } & T;
export type UnknownObjectType = Record<string, unknown>;
export type FormData = Record<string, any>;
export type FormErrors = Record<string, FieldError>;

export type FieldError = {
  message: string;
  values: Record<string, string | number>;
} | UnknownObjectType;
