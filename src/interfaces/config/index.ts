export interface IConfig {
  BASE_API_URL: string;
  WIALON_URL: string;
  LOGIN_URL: string;
}
