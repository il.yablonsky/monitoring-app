export interface ISystemState {
  globalError: Record<string, string | boolean>;
  globalInProgressStatus: boolean;
  is404: boolean;
  redirectTo: string;
  accessToken: string;
}
