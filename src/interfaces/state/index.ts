import { ISystemState } from './system';
import { IUser } from './user';
import { IAuth } from './auth';
import { IUnits } from './units';

export interface IState {
  system: ISystemState;
  user: IUser;
  auth: IAuth;
  units: IUnits;
}
