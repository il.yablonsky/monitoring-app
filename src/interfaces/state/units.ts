import { Nulish } from '..';
import { ICommon } from './common';

export interface IUnitPosition {
  c: number;
  f: number;
  lc: number;
  sc: number;
  s: number;
  t: number;
  x: number;
  y: number;
  z: number;
}

export interface IUnit {
  id: number;
  name: string;
  position: Nulish<IUnitPosition>;
  icon: string;
  lastMessage: Nulish<string>;
  address: Nulish<string>;
  positionChanges: number[]
}

export interface IUnits extends ICommon {
  avlUnits: any[];
  units: Record<number, IUnit>;
}
