export interface ICommon {
  isInProgress: boolean;
  isSuccess: boolean;
}

export interface IFormCommon extends ICommon {
  formData: Record<string, any>;
  errors: Record<string, any>;
}
