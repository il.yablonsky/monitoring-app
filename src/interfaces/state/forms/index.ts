import { ISignInForm } from './sign-in';

export interface IFormsState {
  signIn: ISignInForm;
}
