import { IFormCommon } from '../common';

export interface ISignInFormData {
  userName: string;
  password: string;
}

export interface ISignInForm extends IFormCommon {
  formData: ISignInFormData;
}
