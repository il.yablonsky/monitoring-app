import { ICommon } from './common';

export interface IUser extends ICommon {
  userName: string;
}
