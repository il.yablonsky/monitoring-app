export interface IAuth {
  isAuthorized: boolean;
}
