import * as R from 'ramda';

import { IUnit } from '@/interfaces/state/units';

export const indexById = <T extends { id: number | string }>(arr: T[]) => R.indexBy(R.prop('id'), arr);

const concatByKey = (key: string, leftValue: any, rightValue: any) =>
  key === 'positionChanges' ? R.concat(leftValue, [rightValue]) : rightValue;

export const deepMergeUnitsById = <T>(
  allUnits: Record<number, IUnit>,
  updatedUnits: Record<number, T>,
) => R.mergeDeepWithKey(
    concatByKey,
    allUnits,
    updatedUnits,
  );
