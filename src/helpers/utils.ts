import * as R from 'ramda';

export const isNotNil = R.complement(R.isNil);
export const isNotEmpty = R.complement(R.isEmpty);
export const isEmptyOrNil = R.anyPass([R.isNil, R.isEmpty]);
export const isEmptyOrNilOrFalse = R.anyPass([isEmptyOrNil, R.equals(false)]);
export const zeroOrNil = R.anyPass([R.isNil, R.compose(R.equals(0), Number)]);
export const isFunction = R.is(Function);
export const isBoolean = R.is(Boolean);
export const isObject = R.is(Object);
export const isArray = R.is(Array);

/**
 * Converts passed value to String type (if needed)
 */
export const toString = R.cond([
  [R.isNil, R.always('')],
  [R.is(String), R.identity],
  [R.T, R.toString],
]);
