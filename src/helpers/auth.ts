import store from 'store';

export const getToken = () => store.get('access_token');
