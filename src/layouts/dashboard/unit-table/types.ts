import { IUnit } from '@/interfaces/state/units';

export interface IProps {
  isInProgress: boolean;
  avlUnits: any[];
  units: IUnit[];
}
