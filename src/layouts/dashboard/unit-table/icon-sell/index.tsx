import React, { ChangeEvent } from 'react';
import { useDispatch } from 'react-redux';
import { GridCellParams } from '@material-ui/data-grid';
import * as R from 'ramda';

import { updateUnitIcon } from '@/actions/units';

export const IconCell = (params: GridCellParams) => {
  const dispatch = useDispatch();

  const handleChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    const unitData = { unitId: params.id as number, inputRef: target };

    R.compose(dispatch, updateUnitIcon)(unitData);
  };

  return (
    <>
      <input
        accept="image/*"
        name="upload-icon"
        className="d-none"
        id={`unit-icon-${params.id}`}
        type="file"
        onChange={handleChange}
      />
      {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
      <label className="cursor-pointer" htmlFor={`unit-icon-${params.id}`}>
        <img src={params.value as string} alt="" />
      </label>
    </>
  );
};
