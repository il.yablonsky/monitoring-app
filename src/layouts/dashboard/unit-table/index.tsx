import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { DataGrid, GridEditCellPropsParams } from '@material-ui/data-grid';
import * as R from 'ramda';

import { updateUnitName } from '@/actions/units';
import { IUnit } from '@/interfaces/state/units';
import { IconCell } from './icon-sell';
import { IProps } from './types';

import './index.scss';

const columns = [
  {
    field: 'icon',
    headerName: 'Icon',
    width: 90,
    sortable: false,
    renderCell: IconCell,
  },
  {
    field: 'name',
    headerName: 'Name',
    width: 150,
    editable: true,
  },
  {
    field: 'lastMessage',
    headerName: 'Last message',
    width: 110,
  },
  {
    field: 'speed',
    headerName: 'Speed',
    width: 110,
  },
  {
    field: 'address',
    headerName: 'Address',
    width: 160,
  },
];

const serializeUnits = R.map(({
  id, name, position, icon, lastMessage, address,
}: IUnit) => ({
  id,
  icon,
  name,
  lastMessage,
  speed: position?.s,
  address,
}));

export const UnitTable = ({
  isInProgress,
  units,
}: IProps) => {
  const dispatch = useDispatch();

  const preparedUnits = useMemo(() => serializeUnits(units), [units]);

  const handleEditCellChangeCommitted = ({ id, props: { value } }: GridEditCellPropsParams) => {
    R.compose(dispatch, updateUnitName)({ unitId: id as number, name: value as string });
  };

  return (
    <div className="p-2 unit-list">
      <div>Всего объектов: {units.length}</div>
      <DataGrid
        rows={preparedUnits}
        columns={columns}
        loading={isInProgress}
        onEditCellChangeCommitted={handleEditCellChangeCommitted}
        checkboxSelection
        disableSelectionOnClick
      />
    </div>
  );
};
