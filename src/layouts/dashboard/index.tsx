import React from 'react';
import * as R from 'ramda';

import { Header } from '@/components/header';
import { Main } from '@/components/main';
import { UnitTable } from '@/layouts/dashboard/unit-table';
import { LeafletMap } from '@/components/leaflet-map';
import { useShallowSelector } from '@/hooks/use-shallow-selector';

import './index.scss';

export const Dashboard = () => {
  const {
    isInProgress, avlUnits, units,
  } = useShallowSelector(state => state.units);

  return (
    <Main className="h-100vh dashboard">
      <Header />
      <div className="d-flex h-100 pt-8">
        <UnitTable
          isInProgress={isInProgress}
          avlUnits={avlUnits}
          units={R.values(units)}
        />
        <LeafletMap units={R.values(units)} />
      </div>
    </Main>
  );
};
