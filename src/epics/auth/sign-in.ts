import { ActionType, ofType } from 'deox';
import { Observable, of, concat } from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';
import store from 'store';

import { setUser } from '@/actions/user';
import { signIn, setAuthStatus } from '@/actions/auth';
import { login, getToken } from '@/services/wialon';
import {
  setGlobalInProgressStatusAction,
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof signIn
>;

const loginUser = (accessToken: string) => concat(
  setGlobalInProgressStatusAction(true),
  login(accessToken).pipe(
    mergeMap((user) => {
      const userName = user.getName();

      return of(
        setUser({ userName }),
        setAuthStatus(true),
      );
    }),
  ),
  setGlobalInProgressStatusAction(false),
);

export const signInEpic = (
  action$: Observable<Action>,
) =>
  action$.pipe(
    ofType(signIn),
    switchMap(() => {
      const accessToken = store.get('access_token');

      if (accessToken) {
        return loginUser(accessToken);
      }

      return getToken().pipe(
        mergeMap(loginUser),
      );
    }),
    catchGlobalErrorWithUndefinedId,
  );
