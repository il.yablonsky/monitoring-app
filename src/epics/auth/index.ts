import { combineEpics } from 'redux-observable';

import { signInEpic } from './sign-in';

export const authEpic = combineEpics(
  signInEpic,
);
