import { REDUCER_TYPES } from '@/constants';
import { ActionType, ofType } from 'deox';
import {
  Observable, of, concat, forkJoin,
} from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';

import { setInProgressStatus } from '@/actions/common';
import {
  getAvlUnits,
  setAvlUnits,
  setUnits,
  subscribeUnitIconChange,
  subscribeUnitNameChange,
  subscribeUnitPositionChange,
} from '@/actions/units';
import {
  wialon,
  getUnitLocations,
  getUnits as getUnitsApi,
} from '@/services/wialon';
import { indexById } from '@/helpers/units';
import {
  setInProgressStatusAction,
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof getAvlUnits
>;

export const fetchUnitsEpic = (
  action$: Observable<Action>,
) =>
  action$.pipe(
    ofType(getAvlUnits),
    switchMap(() => concat(
      setInProgressStatusAction(REDUCER_TYPES.UNITS, true),
      getUnitsApi().pipe(
        mergeMap((avlUnits) => forkJoin(
          avlUnits.map((avlUnit) => {
            const position = avlUnit.getPosition();

            return forkJoin({
              id: of(avlUnit.getId()),
              name: of(avlUnit.getName()),
              icon: of(avlUnit.getIconUrl(32)),
              lastMessage: of(wialon.util.DateTime.formatTime(position?.t)),
              position: of(position),
              positionChanges: of([]),
              address: getUnitLocations(position),
            });
          }),
        ).pipe(
          mergeMap((units) => of(
            setAvlUnits(avlUnits),
            setUnits(indexById(units)),
            subscribeUnitPositionChange(),
            subscribeUnitNameChange(),
            subscribeUnitIconChange(),
            setInProgressStatus(REDUCER_TYPES.UNITS, false),
          )),
        )),
      ),
    )),
    catchGlobalErrorWithUndefinedId,
  );
