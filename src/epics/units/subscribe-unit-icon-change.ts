import { ActionType, ofType } from 'deox';
import { Observable, of } from 'rxjs';
import { StateObservable } from 'redux-observable';
import {
  mergeMap, takeUntil, map, switchMap,
} from 'rxjs/operators';
import * as R from 'ramda';

import {
  setUnits, subscribeUnitIconChange, subscribeUnitNameChange, subscribeUnitPositionChange, unsubscribeUnitIconChange,
} from '@/actions/units';
import { deepMergeUnitsById } from '@/helpers/units';
import { IState } from '@/interfaces/state';
import {
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof subscribeUnitIconChange |
  typeof unsubscribeUnitIconChange
>;

type UpdatedUnitType = {
  id: number;
  icon: string;
};

export const subscribeUnitIconChangeEpic = (
  action$: Observable<Action>,
  state$: StateObservable<IState>,
) =>
  action$.pipe(
    ofType(subscribeUnitIconChange),
    switchMap(() => {
      const { units, avlUnits } = state$.value.units;

      return new Observable<UpdatedUnitType>((subscriber) => {
        avlUnits.forEach(avlUnit => {
          avlUnit.addListener('changeIcon', ({ _target }: any) => {
            const data = {
              id: _target.getId(),
              icon: _target.getIconUrl(32),
            } as UpdatedUnitType;

            subscriber.next(data);
          });
        });
      }).pipe(
        mergeMap((updatedUnit) => {
          const mergedUnits = deepMergeUnitsById(units, { [updatedUnit.id]: updatedUnit });

          return of(
            setUnits(mergedUnits),

            subscribeUnitPositionChange(),
            subscribeUnitNameChange(),
          );
        }),
        takeUntil(
          action$.pipe(
            ofType(unsubscribeUnitIconChange),
            map(() => {
              avlUnits.forEach(avlUnit => {
                avlUnit.removeListener('changeIcon', R.__);
              });
            }),
          ),
        ),
      );
    }),
    catchGlobalErrorWithUndefinedId,
  );
