import { combineEpics } from 'redux-observable';

import { fetchUnitsEpic } from './fetch-units';
import { subscribeUnitPositionChangeEpic } from './subscribe-unit-position-change';
import { subscribeUnitIconChangeEpic } from './subscribe-unit-icon-change';
import { subscribeUnitNameChangeEpic } from './subscribe-unit-name-change';
import { updateUnitIconEpic } from './update-unit-icon';
import { updateUnitNameEpic } from './update-unit-name';

export const unitsEpic = combineEpics(
  fetchUnitsEpic,
  subscribeUnitPositionChangeEpic,
  subscribeUnitIconChangeEpic,
  subscribeUnitNameChangeEpic,
  updateUnitIconEpic,
  updateUnitNameEpic,
);
