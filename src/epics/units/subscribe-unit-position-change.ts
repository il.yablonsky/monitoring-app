import { ActionType, ofType } from 'deox';
import { Observable, of, forkJoin } from 'rxjs';
import { StateObservable } from 'redux-observable';
import {
  mergeMap, takeUntil, map, switchMap,
} from 'rxjs/operators';
import * as R from 'ramda';

import {
  setUnits,
  subscribeUnitIconChange,
  subscribeUnitNameChange,
  subscribeUnitPositionChange,
  unsubscribeUnitPositionChange,
} from '@/actions/units';
import { deepMergeUnitsById, indexById } from '@/helpers/units';
import { IUnitPosition } from '@/interfaces/state/units';
import { IState } from '@/interfaces/state';
import { getUnitLocations } from '@/services/wialon';
import {
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof subscribeUnitPositionChange |
  typeof unsubscribeUnitPositionChange
>;

type UnitPositionType = {
  id: number;
  position: IUnitPosition
};

const TIMER = 100;
let interval: NodeJS.Timeout;

export const subscribeUnitPositionChangeEpic = (
  action$: Observable<Action>,
  state$: StateObservable<IState>,
) =>
  action$.pipe(
    ofType(subscribeUnitPositionChange),
    switchMap(() => {
      const { avlUnits, units } = state$.value.units;

      return new Observable<UnitPositionType[]>((subscriber) => {
        const unitPositions = [] as UnitPositionType[];

        avlUnits.forEach(avlUnit => {
          avlUnit.addListener('changePosition', (event: any) => {
            const id = R.path(['_target', '_id'], event) as number;

            unitPositions.push({ id, position: event.getData() });
          });
        });

        if (!interval) {
          interval = setInterval(() => {
            subscriber.next(unitPositions);
            unitPositions.length = 0;
          }, TIMER);
        }
      }).pipe(
        mergeMap((unitPositions) => forkJoin(
          unitPositions.map(({ id, position }) => forkJoin({
            id: of(id),
            position: of(position),
            positionChanges: of([position?.y ?? 0, position?.x ?? 0]),
            address: getUnitLocations(position),
          })),
        ).pipe(
          mergeMap((updatedUnits) => {
            const mergedUnits = deepMergeUnitsById(units, indexById(updatedUnits));

            return of(
              setUnits(mergedUnits),
              subscribeUnitIconChange(),
              subscribeUnitNameChange(),
            );
          }),
        )),
        takeUntil(
          action$.pipe(
            ofType(unsubscribeUnitPositionChange),
            map(() => {
              avlUnits.forEach(avlUnit => {
                avlUnit.removeListener('changePosition', R.__);
              });

              clearInterval(interval);
            }),
          ),
        ),
      );
    }),
    catchGlobalErrorWithUndefinedId,
  );
