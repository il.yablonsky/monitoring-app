import { ActionType, ofType } from 'deox';
import { Observable, of } from 'rxjs';
import { StateObservable } from 'redux-observable';
import {
  mergeMap, takeUntil, map, switchMap,
} from 'rxjs/operators';
import * as R from 'ramda';

import {
  setUnits,
  subscribeUnitIconChange,
  subscribeUnitNameChange,
  subscribeUnitPositionChange,
  unsubscribeUnitNameChange,
} from '@/actions/units';
import { deepMergeUnitsById } from '@/helpers/units';
import { IState } from '@/interfaces/state';
import {
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof subscribeUnitNameChange |
  typeof unsubscribeUnitNameChange
>;

type UpdatedUnitType = {
  id: number;
  name: string;
};

export const subscribeUnitNameChangeEpic = (
  action$: Observable<Action>,
  state$: StateObservable<IState>,
) =>
  action$.pipe(
    ofType(subscribeUnitNameChange),
    switchMap(() => {
      const { units, avlUnits } = state$.value.units;

      return new Observable<UpdatedUnitType>((subscriber) => {
        avlUnits.forEach(avlUnit => {
          avlUnit.addListener('changeName', ({ _target }: any) => {
            const data = {
              id: _target.getId(),
              name: _target.getName(),
            } as UpdatedUnitType;

            subscriber.next(data);
          });
        });
      }).pipe(
        mergeMap((updatedUnit) => {
          const mergedUnits = deepMergeUnitsById(units, { [updatedUnit.id]: updatedUnit });

          return of(
            setUnits(mergedUnits),
            subscribeUnitPositionChange(),
            subscribeUnitIconChange(),
          );
        }),
        takeUntil(
          action$.pipe(
            ofType(unsubscribeUnitNameChange),
            map(() => {
              avlUnits.forEach(avlUnit => {
                avlUnit.removeListener('changeName', R.__);
              });
            }),
          ),
        ),
      );
    }),
    catchGlobalErrorWithUndefinedId,
  );
