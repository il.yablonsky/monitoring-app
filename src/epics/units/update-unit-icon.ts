import { ActionType, ofType } from 'deox';
import { Observable, concat } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { updateUnitIcon } from '@/actions/units';
import { updateUnitIcon as updateUnitIconApi } from '@/services/wialon';
import {
  setGlobalInProgressStatusAction,
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof updateUnitIcon
>;

export const updateUnitIconEpic = (
  action$: Observable<Action>,
) =>
  action$.pipe(
    ofType(updateUnitIcon),
    switchMap(({ payload: { unitId, inputRef } }) => concat(
      setGlobalInProgressStatusAction(true),
      updateUnitIconApi(unitId, inputRef),
      setGlobalInProgressStatusAction(false),
    )),
    catchGlobalErrorWithUndefinedId,
  );
