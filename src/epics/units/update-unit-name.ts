import { ActionType, ofType } from 'deox';
import { Observable, concat } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { updateUnitName } from '@/actions/units';
import { updateUnitName as updateUnitNameApi } from '@/services/wialon';
import {
  setGlobalInProgressStatusAction,
  catchGlobalErrorWithUndefinedId,
} from '../common-operators';

type Action = ActionType<
  typeof updateUnitName
>;

export const updateUnitNameEpic = (
  action$: Observable<Action>,
) =>
  action$.pipe(
    ofType(updateUnitName),
    switchMap(({ payload: { unitId, name } }) => concat(
      setGlobalInProgressStatusAction(true),
      updateUnitNameApi(unitId, name),
      setGlobalInProgressStatusAction(false),
    )),
    catchGlobalErrorWithUndefinedId,
  );
