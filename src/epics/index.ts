import { combineEpics } from 'redux-observable';

import { authEpic } from './auth';
import { unitsEpic } from './units';

export const rootEpic = combineEpics(
  authEpic,
  unitsEpic,
);
