import { Observable } from 'rxjs';
import store from 'store';
import * as R from 'ramda';

import { getConfig } from '@/helpers/env';
import { IWialonUser } from '@/globals';
import { IUnitPosition } from '@/interfaces/state/units';
import { Nulish } from '@/interfaces/index';

export const { wialon } = window;

const baseApiUrl = getConfig<string>('BASE_API_URL');
const loginUrl = getConfig<string>('LOGIN_URL');

export const getToken = () => new Observable<string>((subscriber) => {
  const callback = ({ data }: MessageEvent<string>) => {
    if (R.test(/access_token/, data)) {
      const token = R.replace('access_token=', '', data);

      store.set('access_token', token);

      window.removeEventListener('message', callback);

      subscriber.next(token);
      subscriber.complete();
    }
  };

  window.open(loginUrl as string, '_blank', 'width=760, height=500, top=300, left=500');
  window.addEventListener('message', callback);
});

export const login = (accessToken: string) => new Observable<IWialonUser>((subscriber) => {
  wialon.core.Session.getInstance().initSession(baseApiUrl as string);
  wialon.core.Session.getInstance().loginToken(accessToken, '', (code) => {
    if (code) return;

    const user = wialon.core.Session.getInstance().getCurrUser();

    subscriber.next(user);
    subscriber.complete();
  });
});

export const getUnitLocations = (position: IUnitPosition) => new Observable<Nulish<string>>((subscriber) => {
  wialon.util.Gis.getLocations([{ lon: position?.x, lat: position?.y }], (code, address) => {
    subscriber.next(code ? null : address[0]);
    subscriber.complete();

    return null;
  });
});

export const getUnits = () => new Observable<any[]>((subscriber) => {
  const session = wialon.core.Session.getInstance();
  // eslint-disable-next-line no-bitwise
  const flags = wialon.item.Item.dataFlag.base
  | wialon.item.Unit.dataFlag.lastMessage
  | wialon.item.Item.dataFlag.image;

  session.loadLibrary('itemIcon');
  session.updateDataFlags([{
    type: 'type', data: 'avl_unit', flags, mode: 0,
  }], (code) => {
    if (code) return;
    const units = session.getItems('avl_unit');

    subscriber.next(units);
    subscriber.complete();
  });
});

export const updateUnitIcon = (unitId: number, inputRef: HTMLInputElement) => new Observable((subscriber) => {
  const avlUnit = wialon.core.Session.getInstance().getItem(unitId);

  avlUnit.updateIcon(inputRef, () => {
    subscriber.complete();
  });
});

export const updateUnitName = (unitId: number, unitName: string) => new Observable((subscriber) => {
  const avlUnit = wialon.core.Session.getInstance().getItem(unitId);

  avlUnit.updateName(unitName, () => {
    subscriber.complete();
  });
});
