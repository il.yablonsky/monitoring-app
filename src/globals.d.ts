export {};

export interface IWialonUser {
  getName: () => string;
}

declare global {
  interface Window {
    wialon: {
      core: {
        Session: {
          getInstance: () => ({
            initSession: (url: string) => any;
            loginToken: (accessToken: string, param?: string, callback?: (errorCode: number) => void) => any;
            getCurrUser: () => IWialonUser;
            loadLibrary: (param: string) => void;
            updateDataFlags: (config: any[], callback?: (errorCode: number) => void) => void;
            getItems: (param: string) => any[];
            getItem: (unitId: number) => any;
          }),
        },
        Errors: {
          getErrorText: (errorCode: number) => string;
        },
      },
      item: {
        Item: {
          dataFlag: {
            base: number;
            image: any;
          }
        },
        Unit: {
          dataFlag: {
            lastMessage: number;
          }
        },
      },
      util: {
        DateTime: {
          formatTime: (time: string) => string;
        },
        Gis: {
          getLocations: (
            item: [{ lon: number; lat: number }],
            callback: (code: number, address: string) => void
          ) => void;
        },
      }
    }
  }
}
