import { createActionCreator } from 'deox';

import { IUnit } from '@/interfaces/state/units';

export const getAvlUnits = createActionCreator('GET_UNITS');

export const setAvlUnits = createActionCreator(
  'SET_AVL_UNITS',
  resolve => (avlUnits: any[]) => resolve(avlUnits),
);

export const setUnit = createActionCreator(
  'SET_UNIT',
  resolve => (unit: IUnit) => resolve(unit),
);

export const setUnits = createActionCreator(
  'SET_UNITS',
  resolve => (payload: Record<number, IUnit>) => resolve(payload),
);

export const updateUnitName = createActionCreator(
  'UPDATE_UNIT_NAME',
  resolve => (payload: { unitId: number; name: string }) => resolve(payload),
);

export const updateUnitIcon = createActionCreator(
  'UPDATE_UNIT_ICON',
  resolve => (payload: { unitId: number; inputRef: HTMLInputElement }) => resolve(payload),
);

export const subscribeUnitPositionChange = createActionCreator('SUBSCRIBE_UNIT_POSITION_CHANGE');
export const subscribeUnitIconChange = createActionCreator('SUBSCRIBE_UNIT_ICON_CHANGE');
export const subscribeUnitNameChange = createActionCreator('SUBSCRIBE_UNIT_NAME_CHANGE');

export const unsubscribeUnitIconChange = createActionCreator('UNSUBSCRIBE_UNIT_ICON_CHANGE');
export const unsubscribeUnitPositionChange = createActionCreator('UNSUBSCRIBE_UNIT_POSITION_CHANGE');
export const unsubscribeUnitNameChange = createActionCreator('UNSUBSCRIBE_UNIT_NAME_CHANGE');
