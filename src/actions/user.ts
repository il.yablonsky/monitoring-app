import { createActionCreator } from 'deox';

export const setUser = createActionCreator(
  'SET_USER',
  resolve => (payload: { userName: string }) => resolve(payload),
);
