import { createActionCreator } from 'deox';

export const signIn = createActionCreator('SIGN_IN');

export const signOut = createActionCreator('SIGN_OUT');

export const setAuthStatus = createActionCreator(
  'SET_AUTH_STATUS',
  resolve => (status: boolean) => resolve(status),
);
