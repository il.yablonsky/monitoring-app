import React from 'react';
import { RouteConfig } from 'react-router-config';
import loadable from '@loadable/component';

import { App } from '@/app';
import { PAGES } from '@/constants';

const preloader = {
  fallback: <div>Loading...</div>,
};

const routes = [
  {
    component: App,
    routes: [
      {
        path: '/',
        exact: true,
        component: loadable(() => import(/* webpackChunkName: "dashboard" */ './dashboard'), preloader),
        page: PAGES.DASHBOARD,
      },
      {
        path: '*',
        exact: true,
        component: loadable(() => import(/* webpackChunkName: "page-not-found" */ './page-not-found'), preloader),
      },
    ],
  },
] as RouteConfig[];

export default routes;
