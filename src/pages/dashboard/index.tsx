import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import * as R from 'ramda';

import { clearState } from '@/actions/common';
import { getAvlUnits } from '@/actions/units';
import { Dashboard as DashboardLayout } from '@/layouts/dashboard';
import { REDUCER_TYPES } from '@/constants';

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    R.compose(dispatch, getAvlUnits)();

    return () => {
      R.compose(dispatch, clearState)(REDUCER_TYPES.UNITS);
    };
  }, [dispatch]);

  return <DashboardLayout />;
};

export default Home;
