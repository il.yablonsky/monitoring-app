import React, { useEffect } from 'react';
import { renderRoutes, RouteConfig } from 'react-router-config';
import { useDispatch } from 'react-redux';
import * as R from 'ramda';

import { LinearLoader } from '@/components/progress-bar/linear-loader';
import { signIn } from '@/actions/auth';
import { useShallowSelector } from '@/hooks/use-shallow-selector';

interface IProps {
  route: RouteConfig;
}

export const App = ({
  route: { routes },
}: IProps) => {
  const dispatch = useDispatch();
  const { isAuthorized } = useShallowSelector(state => state?.auth);

  useEffect(() => {
    R.compose(dispatch, signIn)();
  }, [dispatch]);

  return (
    <>
      {isAuthorized ? renderRoutes(routes) : <LinearLoader />}
    </>
  );
};
