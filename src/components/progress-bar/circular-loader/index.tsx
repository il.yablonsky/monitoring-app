import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { IProps } from './types';

export const CircularLoader = ({
  colorPrimary = 'bg-blue',
}: IProps) => (
  <CircularProgress
    classes={{
      root: 'position-absolute circular-loader',
      colorPrimary,
    }}
  />
);
