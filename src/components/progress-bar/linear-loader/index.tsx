import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

import { IProps } from './types';

import './index.scss';

export const LinearLoader = ({
  bgColor = 'bg-blue',
}: IProps) => (
  <LinearProgress
    classes={{
      root: 'w-100 position-fixed linear-loader',
      bar: bgColor,
    }}
  />
);
