import React, { ErrorInfo, PureComponent } from 'react';

import { logError } from '@/helpers/logger';
import { GlobalAlert } from '../global-alert';
import { IProps } from './types';

export class ErrorBoundary extends PureComponent<IProps> {
  state = {
    hasError: false,
  };

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({ hasError: true });

    logError(error, { extra: errorInfo });
  }

  render() {
    const { children, dismissGlobalAlert } = this.props;
    const { hasError } = this.state;

    return hasError
      ? <GlobalAlert dismissGlobalAlert={dismissGlobalAlert} />
      : children;
  }
}
