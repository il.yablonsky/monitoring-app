import React, { memo, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import * as R from 'ramda';

import { useShallowSelector } from '@/hooks/use-shallow-selector';
import { clearErrors } from '@/actions/system';
import { ErrorBoundary } from '@/components/error-boundary';
import { GlobalAlert } from '@/components/global-alert';
import { LinearLoader } from '@/components/progress-bar/linear-loader';
import { isEmptyOrNil } from '@/helpers/utils';
import { IProps } from './types';

export const MainUI = ({
  children,
  className = null,
  disableGutters = false,
}: IProps) => {
  const {
    globalInProgressStatus,
    globalError: { errorId },
  } = useShallowSelector(state => state?.system);
  const { isAuthorized } = useShallowSelector(state => state.auth);
  const dispatch = useDispatch();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const dismissGlobalAlert = useCallback(
    R.compose(dispatch, clearErrors),
    [dispatch],
  );

  return (
    <Container
      disableGutters={disableGutters}
      maxWidth={false}
      className={classnames('p-0', className)}
    >
      {!isEmptyOrNil(errorId) && (
        <GlobalAlert
          errorId={errorId as string}
          dismissGlobalAlert={dismissGlobalAlert}
        />
      )}

      <ErrorBoundary dismissGlobalAlert={dismissGlobalAlert}>
        <>
          {globalInProgressStatus && <LinearLoader />}
          {isAuthorized ? children : <LinearLoader />}
        </>
      </ErrorBoundary>
    </Container>
  );
};

export const Main = memo(MainUI);
