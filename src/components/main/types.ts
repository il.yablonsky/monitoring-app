import { Nulish } from '@/interfaces/index';

export interface IProps {
  children: JSX.Element | JSX.Element[];
  className?: Nulish<string>;
  disableGutters?: boolean;
  showSideBar?: boolean;
}
