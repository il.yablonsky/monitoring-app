import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { useShallowSelector } from '@/hooks/use-shallow-selector';

export const Header = () => {
  const { userName } = useShallowSelector(state => state.user);

  return (
    <AppBar position="fixed">
      <Toolbar className="d-flex justify-content-between">
        <Typography variant="h6">
          Wialon
        </Typography>
        <Typography>{userName}</Typography>
      </Toolbar>
    </AppBar>
  );
};
