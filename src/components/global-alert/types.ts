export interface IProps {
  dismissGlobalAlert: () => void;
  errorId?: string;
}
