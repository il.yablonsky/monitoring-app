import React, { Fragment } from 'react';
import { LatLngTuple, Icon, LatLngExpression } from 'leaflet';
import {
  MapContainer, TileLayer, Marker, Tooltip, Polyline,
} from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';

import { IProps } from './types';

import './index.scss';

const mapPosition = [53.9, 27.55] as LatLngTuple;

export const LeafletMap = ({
  units,
}: IProps) => (
  <div className="leaflet-conitainer">
    <MapContainer className="h-100" center={mapPosition} zoom={10} scrollWheelZoom={false}>
      <TileLayer
        url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
        attribution={'&copy; <a href="http://gurtam.com">Gurtam</a>'}
      />
      {/* @ts-ignore */}
      <MarkerClusterGroup>
        {units.map(({
          id,
          name,
          position,
          positionChanges,
          icon,
        }) => (
          position && (
            <Fragment key={id}>
              <Marker
                position={[position.y ?? 0, position.x ?? 0]}
                icon={new Icon({ iconUrl: icon })}
              >
                <Tooltip>{name}</Tooltip>
              </Marker>
              <Polyline
                pathOptions={{ color: `#${id.toString(16)}` }}
                positions={positionChanges as unknown as LatLngExpression[]}
              />
            </Fragment>
          )
        ))}
      </MarkerClusterGroup>
    </MapContainer>
  </div>
);
