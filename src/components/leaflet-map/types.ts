import { IUnit } from '@/interfaces/state/units';

export interface IProps {
  units: IUnit[];
}
