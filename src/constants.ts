/* eslint-disable no-shadow */

export const PROJECT_ENVS = {
  DEV: 'development',
  PROD: 'production',
};

export const IS_MODE_DEV = process.env.NODE_ENV === PROJECT_ENVS.DEV;

export enum AJAX_METHODS {
  POST = 'post',
  PUT = 'put',
  PATCH = 'patch',
  GET = 'get',
  GET_JSON = 'getJSON',
  DELETE = 'delete',
}

export enum FORM_FIELD_TYPES {
  TEXT = 'text',
  TEXTAREA = 'textarea',
  PASSWORD = 'password',
  EMAIL = 'email',
  SWITCH = 'switch',
  RADIO = 'radio',
  CHECKBOX = 'checkbox',
  SELECT = 'select',
  DATE = 'date',
}

export const FORM_TARGET_VALUES_SERIALIZE = {
  [FORM_FIELD_TYPES.TEXT]: 'value',
  [FORM_FIELD_TYPES.TEXTAREA]: 'value',
  [FORM_FIELD_TYPES.PASSWORD]: 'value',
  [FORM_FIELD_TYPES.EMAIL]: 'value',
  [FORM_FIELD_TYPES.SELECT]: 'value',
  [FORM_FIELD_TYPES.CHECKBOX]: 'checked',
  [FORM_FIELD_TYPES.RADIO]: 'checked',
  [FORM_FIELD_TYPES.SWITCH]: 'checked',
};

export enum ERRORS {
  UNDEFINED = 'UNKNOWN_ERROR',
  AUTH_ERROR = 'AUTH_ERROR',
}

export enum PAGES {
  DASHBOARD = 'dashboard',
  SIGN_IN = 'sign-in',
}

export enum REDUCER_TYPES {
  USER = 'user',
  AUTH = 'auth',
  UNITS = 'units'
}
