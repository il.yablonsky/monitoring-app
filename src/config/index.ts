export const CONFIG = {
  BASE_API_URL: 'https://hst-api.wialon.com',
  WIALON_URL: 'https://hosting.wialon.com',
  LOGIN_URL: 'https://hosting.wialon.com/login.html?client_id=App&access_type=0x100&activation_time=0&duration=604800&flags=0x1&redirect_uri=https://hosting.wialon.com/post_token.html',
};
