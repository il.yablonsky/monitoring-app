# @Simple Monitoring App

## How to use locally ?

1. Install ```yarn``` for your OS - https://yarnpkg.com/en/
2. Use ```yarn``` in root directory to install all packages
3. Use ```yarn start:dev`` on *unix OS
